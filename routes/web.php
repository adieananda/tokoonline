<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**/ 
/*FE*/ 
Route::get('/', function () {
    return view('fe.index');
});


/*BE*/ 
	Auth::routes();
	Route::get('/home', 'HomeController@index')->name('home');
	Route::resource('user', 'User\UserController');
	Route::post('user/update/{s}', 'User\UserController@update');
	Route::get('user/delete/{s}', 'User\UserController@destroy');	
	Route::resource('roles', 'User\RolesController');
	Route::post('roles/update/{s}', 'User\RolesController@update');
	Route::get('roles/delete/{s}', 'User\RolesController@destroy');
	Route::resource('product', 'Product\ProductController');
	Route::get('product/delete/{s}', 'Product\ProductController@destroy');
	Route::post('product/update/{s}', 'Product\ProductController@update');
	Route::resource('order', 'Order\OrderController');
	Route::post('order/addproduct', 'Order\OrderController@addproduct');
	Route::get('order/deleteproduct/{s}/{d}', 'Order\OrderController@deleteproduct');
	Route::post('order/update/{s}', 'Order\OrderController@update');
	Route::get('order/delete/{s}', 'Order\OrderController@destroy');
	Route::get('order/forward/{s}', 'Order\OrderController@forward');
	Route::get('order/invoice/{s}', 'Order\OrderController@invoice');
	Route::get('order/download/{s}', 'Order\OrderController@pdf');
	Route::get('order/scan/{s}', 'Order\OrderController@scan');
	Route::get('order/status/{id}/{status}', 'Order\OrderController@status');
	Route::resource('vendors', 'Vendor\VendorController');
	Route::get('vendors/delete/{s}', 'Vendor\VendorController@destroy');
	Route::post('vendors/update/{s}', 'Vendor\VendorController@update');
	Route::resource('uom', 'Uom\UomController');
	Route::post('uom/update/{s}', 'Uom\UomController@update');
	Route::get('uom/delete/{s}', 'Uom\UomController@destroy');

	
