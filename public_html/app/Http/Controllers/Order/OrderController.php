<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;

class OrderController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*store order*/ 
        $data                   = $request->all();
        $data['shipment_date']  = \Carbon\Carbon::parse($request->shipment_date)->toDateTimeString();
        $data['qty_total']     = str_replace(",", "", $request->qty_total);
        $data['status']         = 1;/*pending*/
        $data['created_by']     = \Auth::user()->id;
        $store                  = Order::create($data);
        $orderno                = self::gOrderNo($request->product_id,$store->id,$request->vendor_id);
        $update                 = Order::findOrFail($store->id)->update(['no_order' => $orderno]);
        /*end store order*/ 
        return redirect()->route('order.index');

    }

    public static function gOrderNo($code_product,$number,$vendorid)
    {
       $order_no = "#DX0".$code_product.$number.$vendorid;
       return $order_no;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Order::findOrFail($id);
        return view('order.detail',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forward($id)
    {
        $order = Order::findOrFail($id);
        Mail::to('ivanovadieananda@gmail.com')->send(new OrderShipped($order));
        // Mail::to($order->hasVendor->email)->send(new OrderShipped($order));
        $order->update(['status' => 3]);
        return redirect()->route('order.show',$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data                   = $request->all();
        $data['qty_total']      = str_replace(",", "", $request->qty_total);
        $update = Order::findOrFail($id)->update($data);
        return redirect()->route('order.show',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /* cancel order == 2*/ 
        $cancel      = Order::findOrFail($id)->update(['status' => 2]);
        /* end cancel order */ 
        return redirect()->route('order.show',$id);
    } 

    public function invoice($id)
    {
        $data = Order::findOrFail($id);
        $pdf = \PDF::loadView('templates/invoice',['data'=>$data]);
        $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf->setPaper('A4', 'portrait');
        
        return $pdf->download($data->no_order.'.pdf');
    }
}
