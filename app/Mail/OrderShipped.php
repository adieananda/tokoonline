<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;

      /**
     * The order instance.
     *
     * @var Order
     */
    public $order;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }   

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        $pdf = \PDF::loadView('templates/invoice',['data' => $this->order]);
        $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf->setPaper('A4', 'portrait');

        return$this->subject('Order Shipped PT DEATEXS')
                    ->view('emails.order')
                    ->with(['data' => $this->order])
                    ->attachData($pdf->output(), $this->order['no_order'].'.pdf', [
                        'mime' => 'application/pdf',
                    ]);
    }
}
