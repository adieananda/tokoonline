<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        //  if (config('app.debug') && !($exception instanceof ValidationException) && !($exception instanceof HttpResponseException)) {
            
        //     return \Response::view('errors.error', ['code' => 500, 'icon' => 'exclamation-triangle', 'message' => "We're sorry, Internal server error."]);
        // }

        // if($exception instanceof \Symfony\Component\Debug\Exception\FatalErrorException) {
            
        //     return \Response::view('errors.error', ['code' => 500, 'icon' => 'exclamation-triangle', 'message' => "We're sorry, Internal server error."]);
        // }

        // if($this->isHttpException($exception))
        // {
        //     switch (intval($exception->getcode())) {
        //         case 400:
                    
        //             return \Response::view('errors.error', ['code' => 400, 'icon' => 'exclamation-triangle', 'message' => "We're sorry, Bad request."]);
        //             break;
        //         case 401:
                    
        //             return \Response::view('errors.error', ['code' => 401, 'icon' => 'ban', 'message' => "We're sorry, you don't have any permission to access this page."]);
        //             break;
        //         case 403:
                    
        //             return \Response::view('errors.error', ['code' => 403, 'icon' => 'ban', 'message' => "We're sorry, you don't have any permission to access this page."]);
        //             break;
        //         case 404:
                    
        //             return \Response::view('errors.error', ['code' => 404, 'icon' => 'file', 'message' => "We're sorry, We're sorry, but the page you were looking for doesn't exist.."]);
        //             break;
        //         case 405:
                    
        //             return \Response::view('errors.error', ['code' => 405, 'icon' => 'exclamation-triangle', 'message' => "We're sorry, Method not allowed."]);
        //             break;
        //         case 408:
                    
        //             return \Response::view('errors.error', ['code' => 408, 'icon' => 'exclamation-triangle', 'message' => "We're sorry, Request Time-out."]);
        //             break;
        //         case 500:
                    
        //             return \Response::view('errors.error', ['code' => 500, 'icon' => 'exclamation-triangle', 'message' => "We're sorry, Internal server error."]);
        //             break;
        //         case 502:
                    
        //             return \Response::view('errors.error', ['code' => 502, 'icon' => 'exclamation-triangle', 'message' => "We're sorry, Bad Gateway."]);
        //             break;
        //         case 503:
                    
        //             return \Response::view('errors.error', ['code' => 503, 'icon' => 'exclamation-triangle', 'message' => "We're sorry, Service Unavailable."]);
        //             break;
        //         case 504:
                    
        //             return \Response::view('errors.error', ['code' => 504, 'icon' => 'exclamation-triangle', 'message' => "We're sorry, Gateway Time-out."]);
        //             break;

        //         default:
        //             return $this->renderHttpException($exception);
        //             break;
        //     }
        // }
        // else
        // {
        //    return parent::render($request, $exception);

        // }
        
           return parent::render($request, $exception);


    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
