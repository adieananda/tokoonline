<?php

namespace App\Http\Controllers\Vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Vendor;

class VendorController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        return view('vendors.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vendors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        try{
        /*STORE DATA*/
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone_no' => 'required|max:12',
            'address' => 'required|max:225',
            'desc' => 'max:225',
            'zip_code' => 'required',
            'city' => 'required',
            'country' => 'required',
        ]);
        $store          = Vendor::create($request->all());
        \Session::flash('success', 'Success Add Supplier!'); 

        }catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('vendors.index');
        
        /*END STORE DATA*/ 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $data = Vendor::findOrFail($id);
          return view('vendors.edit',compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
          $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'phone_no' => 'required|max:12',
            'address' => 'required|max:225',
            'desc' => 'max:225',
            'zip_code' => 'required',
            'city' => 'required',
            'country' => 'required',
        ]);
         $update = Vendor::findOrFail($id)->update($request->all());
          \Session::flash('success', 'Success Update Supplier!'); 

        }catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
         return redirect()->route('vendors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
        $delete = Vendor::findOrFail($id)->delete();
          \Session::flash('success', 'Success Delete Supplier!'); 

        }catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('vendors.index');
    }
}
