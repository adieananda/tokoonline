<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;

class OrderController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('order.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
        /*store order*/ 
          $this->validate($request, [
            'no_order' => 'required',
            'shipment_date' => 'required',
            'vendor_id' => 'required',
        ]);
        $data                   = $request->all();
        $data['shipment_date']  = \Carbon\Carbon::parse($request->shipment_date)->toDateTimeString();
        $data['status']         = 1;/*pending*/
        $data['created_by']     = \Auth::user()->id;
        $store                  = Order::create($data);
        /*end store order*/ 
         \Session::flash('success', 'Success Add Order!'); 
    
            /*END STORE DATA*/ 
        
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('order.index');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Order::findOrFail($id);
        return view('order.detail',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forward($id)
    {
        try{
        $order = Order::findOrFail($id);
            Mail::to($order->hasVendor->email)->send(new OrderShipped($order));
            $order->update(['status' => 3]);
         \Session::flash('success', 'Success Send Order!'); 
    
            /*END STORE DATA*/ 
        
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('order.show',$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
          $this->validate($request, [
            'shipment_date' => 'required',
            'vendor_id' => 'required',
        ]);

        $data                   = $request->all();
        $data['shipment_date']  = \Carbon\Carbon::parse($request->shipment_date)->toDateTimeString();
        $update = Order::findOrFail($id)->update($data);
         \Session::flash('success', 'Success Update Order!'); 
    
            /*END STORE DATA*/ 
        
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('order.show',$id);
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /* cancel order == 2*/ 
        $cancel      = Order::findOrFail($id)->update(['status' => 2]);
        /* end cancel order */ 
        return redirect()->route('order.show',$id);
    } 

    public function invoice($id)
    {
        $data = Order::findOrFail($id);
       return view('templates.print',compact('data'));
    }
    public function pdf($id)
    {
        $data = Order::findOrFail($id);
        $pdf = \PDF::loadView('templates/invoice',['data' => $data]);
        $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf->setPaper('A4', 'portrait');
        return $pdf->download($data->no_order.'.pdf');
    }

    public function addproduct(Request $request)
    {
        try{
        
        $data                   = $request->all();
        $data['qty_total']      = str_replace(",", "", $request->qty_total);
        $data['status']         = 3;
        $store                  = OrderDetail::create($data);
        /* Update Order Detail*/

        $update                 = OrderDetail::findOrFail($store->id)->update(['code' => uniqid()]);

        /* End Update Order Detail */ 
         \Session::flash('success', 'Success Add Product Order!'); 
        
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('order.show',$request->order_id);
    }
    public function deleteproduct($id,$idorder)
    {
        try{
        $cancel      = OrderDetail::findOrFail($id)->delete();
     
        \Session::flash('success', 'Success Delete Product Order!'); 
        
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('order.show',$idorder);
    } 

    public function scan($orderno)
    { 
     
        $order      = Order::where('no_order','LIKE',$orderno)->first();
        return $order; 
    } 
    
    public function status($id,$status)
    { 
        try{
        $order      = Order::findOrFail($id)->update(['status' => $status]);
         \Session::flash('success', 'Success Change Status Order!'); 
        
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('order.show',$id); 
    } 
}
