<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Permission;
use App\Models\Roles;
use App\Models\RolesPermission;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.roles.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $output = [];
        foreach (Permission::all() as $key => $value) {
           $char  = explode('_', $value->name);
           $arr = array('id' => $value->id,'desc' => $value->desc,'name' => $value->name);
            if(!array_key_exists($char[0], $output)){
                $output[$char[0]][] = $arr;
            }else{

             $output[$char[0]][] = $arr;
            }
   
        }
        // return $output;
        return view('auth.roles.create',compact('output'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
        /*Store Roles*/ 
        $data  = $request->all();
        $roles = Roles::create($request->all());
        $roles_perm = [];
        foreach ($request->roles_permission as $key => $value) {

            $str = explode("-",$value);
            $roles_permission = RolesPermission::create(['roles_id' => $roles->id,'permission_id' => $str[0]]);
            $roles_perm[$key] = $str[1];
        }
        /*update roles access*/
        $new['access_permission'] = json_encode(array('data'=>$roles_perm));
        $update = Roles::findOrFail($roles->id)->update($new);
         \Session::flash('success', 'Success Add Roles!'); 
    
            /*END STORE DATA*/ 
        
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('roles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $output = [];
        foreach (Permission::all() as $key => $value) {
           $char  = explode('_', $value->name);
           $arr = array('id' => $value->id,'desc' => $value->desc,'name' => $value->name);
            if(!array_key_exists($char[0], $output)){
                $output[$char[0]][] = $arr;
            }else{

             $output[$char[0]][] = $arr;
            }
   
        }
        $data        = Roles::findOrFail($id);
        $permission  = $data->hasRolesPermission->pluck('permission_id')->toArray();
        return view('auth.roles.edit',compact('data','permission','output'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
        $upt_roles = Roles::findOrFail($id);
        $upt_roles->hasRolesPermission()->delete();
        $upt_roles->update($request->all());
        $roles_perm = [];
        foreach ($request->roles_permission as $key => $value) {
            $str = explode("-",$value);
            $roles_permission = RolesPermission::create(['roles_id' => $id,'permission_id' => $str[0]]);
            $roles_perm[$key] = $str[1];
        }
        $new['access_permission'] = json_encode(array('data'=>$roles_perm));
        $update = Roles::findOrFail($id)->update($new);
        \Session::flash('success', 'Success Update Roles!'); 
    
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('roles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
        /* Remove Roles*/
        $rmv_roles = Roles::findOrFail($id);
        $rmv_roles->hasRolesPermission()->delete();
        $rmv_roles->delete();
        \Session::flash('success', 'Success Delete Roles!'); 
    
        
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('roles.index');
      
    }


}
