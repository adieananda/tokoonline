<?php

namespace App\Http\Controllers\Uom;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Uom;

class UomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('uom.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('uom.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
          $this->validate($request, [
            'name' => 'required',
        ]);
        $uom = Uom::create($request->all());
        \Session::flash('success', 'Success Add Uom!'); 

        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('uom.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Uom::findOrFail($id);
        return view('uom.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
          $this->validate($request, [
            'name' => 'required',
        ]);
        $uom = Uom::findOrFail($id)->update($request->all());
         \Session::flash('success', 'Success Update Uom!'); 

        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('uom.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
         $uom = Uom::findOrFail($id)->delete();
        \Session::flash('success', 'Success Delete Uom!'); 

        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        return redirect()->route('uom.index');
    }
}
