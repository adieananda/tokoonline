<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('product.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
                
            /*STORE DATA*/

            $this->validate($request, [
                'name' => 'required',
                'code' => 'required',
                'uom' => 'required',
                'desc' => 'max:225',
            ]);

            $store          = Product::create($request->all());
            \Session::flash('success', 'Success Add Product!'); 
    
            /*END STORE DATA*/ 
        
        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        
        return redirect()->route('product.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data = Product::findOrFail($id);
        return view('product.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

             $this->validate($request, [
                'name' => 'required',
                'code' => 'required',
                'uom' => 'required',
                'desc' => 'max:225',
            ]);
            $update = Product::findOrFail($id)->update($request->all());
            \Session::flash('success', 'Success Update Product!'); 

        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }
        
        return redirect()->route('product.index');


    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $delete = Product::findOrFail($id)->delete();
            \Session::flash('success', 'Success Delete Product!'); 


        }
        catch (\Exception $e) {
            
            \Session::flash('error', $e->getMessage()); 
        
        }

        return redirect()->route('product.index');
    }
}
