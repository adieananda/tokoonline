<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_detail';
     
     protected $fillable = [
        'order_id', 'qty_satuan', 'qty_global','qty_total','product_id','inch','width','height','notes','uom','created_by','status','code'
    ];

    public function hasProduct(){

    	 return $this->belongsTo('App\Models\Product','product_id','id')->withTrashed();
    } 
    public function hasOrder(){

    	 return $this->belongsTo('App\Models\Order','order_id','id');
    }  
    public function hasuom(){

    	 return $this->belongsTo('App\Models\Uom','uom','id')->withTrashed();
    } 
    public function hasCreator(){
        return $this->belongsTo('App\User','created_by','id')->withTrashed();
    }
     public function hasStatus(){

         return $this->belongsTo('App\Models\StatusOrder','status','id');
    }
}
