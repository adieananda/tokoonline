<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RolesPermission extends Model
{
     protected $table = 'roles_permission';
     
     protected $fillable = [
        'roles_id','permission_id'
    ];

     public function hasPermission(){

         return $this->belongsTo('App\Models\Permission','permission_id','id');
    }
    public function hasAccess(){
     
        return $this->hasPermission();
    }
}
