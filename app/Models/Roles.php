<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';
     
     protected $fillable = [
        'name','access_permission'
    ];

    public function hasRolesPermission(){
         return $this->hasMany('App\Models\RolesPermission','roles_id','id');
    } 
  
 
   	
}
