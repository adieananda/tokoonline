<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
    use SoftDeletes;
    protected $table = 'vendor';
    protected $dates = ['deleted_at'];
     protected $fillable = [
        'name', 'address', 'zip_code','phone_no','email','city','country','desc'
    ];
}
