<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Product extends Model
{
    use SoftDeletes;
     protected $table = 'products';
     protected $dates = ['deleted_at'];
     protected $fillable = [
        'code', 'name', 'desc','uom'
    ];

     public function hasUom(){

    	 return $this->belongsTo('App\Models\Uom','uom','id');
    }

}
