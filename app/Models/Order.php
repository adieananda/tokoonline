<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $table = 'order';
     
     protected $fillable = [
        'no_order','status','notes','vendor_id','shipment_date','created_by'
    ];

    public function hasVendor(){

         return $this->belongsTo('App\Models\Vendor','vendor_id','id')->withTrashed();
    }
    public function hasStatus(){

         return $this->belongsTo('App\Models\StatusOrder','status','id');
    }
    public function hasOrderDetail(){

    	 return $this->hasMany('App\Models\OrderDetail','order_id','id');
    }
}
