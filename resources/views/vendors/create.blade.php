@extends('layouts.admin')
@section('content')
   <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Create Supplier</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('vendors.index')}}">Supplier</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Create Supplier</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

	  <div class="card">

            <div class="card-body">
           			 <form class="form-horizontal" method="POST" action="{{ route('vendors.store') }}">
           			 	   {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Supplier Name</label>
                                        <div class="col-sm-9">
                                            <input type="text"  name="name" class="form-control{{ $errors->has('name') ? ' is-inavlid' : '' }}" id="lname" placeholder="Ex:PT.XYZ" >
                                            @if ($errors->has('name'))
	                                          <div class="invalid-feedback">
	                                            {{ $errors->first('name') }}
	                                        </div>
	                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">E-Mail</label>
                                        <div class="col-sm-9">
                                            <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="lname" placeholder="Ex:xxx@xxx.com">
                                             @if ($errors->has('email'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Phone No</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="phone_no" onkeypress="return isNumberKey(event)" class="form-control{{ $errors->has('phone_no') ? ' is-invalid' : '' }}" id="lname" placeholder="Ex:081XXXXXX">
                                             @if ($errors->has('phone_no'))
	                                            <div class="invalid-feedback">
	                                            {{ $errors->first('phone_no') }}
	                                        </div>
	                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Address</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address"></textarea>
	                                            @if ($errors->has('address'))
	                                            <div class="invalid-feedback">
	                                            {{ $errors->first('address') }}
	                                        </div>
	                                        @endif

                                        </div>
                                    </div>
                                     
                                      <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Zip Code</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="zip_code" onkeypress="return isNumberKey(event)" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" id="lname" placeholder="Ex: 41211">
                                             @if ($errors->has('zip_code'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('zip_code') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                      <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">City</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="city" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" id="lname" placeholder="EX:Jakarta">
                                             @if ($errors->has('city'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('city') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                      <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Country</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="country" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" id="lname" placeholder="EX:Indonesia">
                                             @if ($errors->has('country'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('country') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control{{ $errors->has('desc') ? ' is-invalid' : '' }}" name="desc" placeholder=""></textarea>
                                                @if ($errors->has('desc'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('desc') }}
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>

            </div>
        </div>
@endsection
@section('js')
<script type="text/javascript">
     /* validate type number*/

    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }

    /* end validate type number*/ 
</script>
@endsection