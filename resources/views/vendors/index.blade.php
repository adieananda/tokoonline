@extends('layouts.admin')
@section('css')
<link href="{{ url('public/admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
       <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Supplier</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Supplier</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
	       <div class="card">
                        <div class="card-body">
                             @if(in_array('vendor_create',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                        		<a href="{{route('vendors.create')}}"><button class="btn btn-primary btn-md"><i class="fas fa-plus-square"></i> Add Supplier</button></a>
	                    	  @endif
                            <hr>
                            <div class="table-responsive">
                                <table id="zero_config" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Zip Code</th>
                                            <th>Phone No</th>
                                            <th>E-Mail</th>
                                            <th>City</th>
                                            <th>Country</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(App\Models\Vendor::all() as $index=>$row)
                               	        <tr>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->address}}</td>
                                            <td>{{$row->zip_code}}</td>
                                            <td>{{$row->phone_no}}</td>
                                            <td>{{$row->email}}</td>
                                            <td>{{$row->city}}</td>
                                            <td>{{$row->country}}</td>
                                            <td>{{$row->created_at}}</td>
                                            <td>
                                                @if(in_array('vendor_delete',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                                <a href="{{ url('vendors/delete/')}}/{{$row->id}}" ><button class="btn btn-danger btn-md"><i class="fas fa-trash"></i></button></a> 
                                                @endif
                                                 @if(in_array('vendor_edit',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                                <a href="{{route('vendors.edit',$row->id)}}"><button class="btn btn-primary btn-md"><i class="fas fa-edit"></i></button></a></td>
                                                @endif
                                        </tr>
                                        @endforeach
                                    </tbody>
                               
                                </table>
                            </div>

                        </div>
                    </div>

@endsection
@section('js')
<script src="{{ url('public/admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
	$('#zero_config').DataTable();
</script>
@endsection