@extends('layouts.admin')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ url('public/admin/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
       <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Order Detail</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('order.index')}}">Order</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Order Detail</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
             <div class="container-fluid">
                <div class="row">
                  <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                               <div class="row">
                                            @if($data->status == 1)
                                            @if(in_array('order_forward',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                            <div class="col-md-2">
                                                <div class="card card-hover">
                                                    <div class="box bg-success text-center" id="ForwardOrder">
                                                        <h1 class="font-light text-white"><i class="fas fa-paper-plane"></i></h1>
                                                        <h6 class="text-white">Send</h6>
                                                    </div>
                                                </div>
                                            </div> 
                                            @endif
                                                @if(in_array('order_print',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                            <div class="col-md-2" >
                                                <div class="card card-hover">
                                                    <a href="{{ url('order/invoice') }}/{{ $data->id }}" target="_blank"> 
                                                    <div class="box bg-primary text-center" >
                                                        <h1 class="font-light text-white"><i class="fas fa-print"></i></h1>
                                                        <h6 class="text-white">Print</h6>
                                                    </div>
                                                    </a>
                                                </div>
                                            </div>
                                            @endif
                                            @if(in_array('order_download',json_decode(\Auth::user()->hasRoles->access_permission)->data)) 
                                            <div class="col-md-2" >
                                                <div class="card card-hover">
                                                    <a href="{{ url('order/download') }}/{{ $data->id }}" target="_blank"> 
                                                    <div class="box bg-primary text-center" >
                                                        <h1 class="font-light text-white"><i class="mdi mdi-file-pdf"></i></h1>
                                                        <h6 class="text-white">PDF</h6>
                                                    </div>
                                                    </a>
                                                </div>
                                            </div>
                                            @endif 
                                          <!--   @if(in_array('order_cancel',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                            <div class="col-md-2" >
                                                <div class="card card-hover">
                                                    <div class="box bg-warning text-center" id="CancelOrder">
                                                        <h1 class="font-light text-white"><i class="fas fa-times"></i></h1>
                                                        <h6 class="text-white">Cancel Order</h6>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif -->
                                            @if(in_array('order_edit',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                            <div class="col-md-2" >
                                                <div class="card card-hover">
                                                    <div class="box bg-primary text-center" id="editOrder">
                                                        <h1 class="font-light text-white"><i class="fas fa-edit"></i></h1>
                                                        <h6 class="text-white">Edit Order</h6>
                                                    </div>
                                                </div>
                                            </div> 
                                            @endif
                                            @endif
                                         
                                            @if($data->status == 3)
                                            @if(in_array('order_print',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                            <div class="col-md-2" >
                                                <div class="card card-hover">
                                                    <a href="{{ url('order/invoice') }}/{{ $data->id }}" target="_blank"> 
                                                    <div class="box bg-primary text-center" >
                                                        <h1 class="font-light text-white"><i class="fas fa-print"></i></h1>
                                                        <h6 class="text-white">Print</h6>
                                                    </div>
                                                    </a>
                                                </div>
                                            </div>
                                            @endif
                                            @if(in_array('order_download',json_decode(\Auth::user()->hasRoles->access_permission)->data)) 
                                            <div class="col-md-2" >
                                                <div class="card card-hover">
                                                    <a href="{{ url('order/download') }}/{{ $data->id }}" target="_blank"> 
                                                    <div class="box bg-primary text-center" >
                                                        <h1 class="font-light text-white"><i class="mdi mdi-file-pdf"></i></h1>
                                                        <h6 class="text-white">PDF</h6>
                                                    </div>
                                                    </a>
                                                </div>
                                            </div>
                                            @endif
                                           <!--  @if(in_array('order_cancel',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                            <div class="col-md-2" >
                                                <div class="card card-hover">
                                                    <div class="box bg-warning text-center" id="CancelOrder">
                                                        <h1 class="font-light text-white"><i class="fas fa-times"></i></h1>
                                                        <h6 class="text-white">Cancel Order</h6>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif -->
                                            @if(in_array('order_cancel',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                            <div class="col-md-2" >
                                                <div class="card card-hover">
                                                    <a href="{{ url('order/status') }}/{{ $data->id }}/4"> 
                                                    <div class="box bg-primary text-center" >
                                                        <h1 class="font-light text-white"><i class="mdi mdi-checkbox-multiple-marked-outline"></i></h1>
                                                        <h6 class="text-white">Complete Order</h6>
                                                    </div>
                                                    </a>
                                                </div>
                                            </div>
                                            @endif
                                            @endif
                                             @if($data->status == 4)
                                            <div class="col-md-2" >
                                                <div class="card card-hover">
                                                    <div class="box bg-success text-center" >
                                                        <h1 class="font-light text-white"><i class="fas fa-check-circle"></i></h1>
                                                        <h6 class="text-white">COMPLETE</h6>
                                                    </div>
                                                    </div>
                                            </div>
                                            @endif
                                           <!--  <div class="col-sm-4" >
                                                   <img style="float:right;" src="data:image/png;base64,{{DNS1D::getBarcodePNG($data->no_order,'C93',4,90)}}" alt="barcode" />
                                            </div> -->

                                    </div>
                                </div>
                         </div>
                    </div>
                     <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"><b>Order {{$data->no_order}}</b></h5>
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Created By</label>
                                        <div class="col-sm-9">
                                            @php
                                            $user = App\User::findOrFail($data->created_by);
                                         
                                            @endphp
                                               <input type="text"  class="form-control"  value="{{$user->name}} / {{$user->emp_code}}" readonly="">
                                        </div>
                                </div>
                            
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Created At</label>
                                        <div class="col-sm-9">
                                           
                                           
                                               <input type="text"  class="form-control"  value="{{$data->created_at}}" readonly="">
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Shipment Date</label>
                                        <div class="col-sm-9">
                                           
                                               <input type="text"  class="form-control"  value="{{$data->shipment_date}}" readonly="">
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Status</label>
                                        <div class="col-sm-9">
                                           
                                               <input type="text"  class="form-control"  value="{{$data->hasStatus->name}}" readonly="">
                                        </div>
                                </div>
                         
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Vendor</h5>
                                 <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                        <div class="col-sm-9">
                                           
                                               <input type="text"  class="form-control"  value="{{$data->hasVendor->name or null}}" readonly="">
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Shipment Date</label>
                                        <div class="col-sm-9">
                                           
                                               <textarea rows="3" class="form-control" readonly="">{{$data->hasVendor->address or null}}</textarea>
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Zip Code</label>
                                        <div class="col-sm-9">
                                           
                                               <input type="text"  class="form-control"  value="{{$data->hasVendor->zip_code or null}}" readonly="">
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">City</label>
                                        <div class="col-sm-9">
                                           
                                               <input type="text"  class="form-control"  value="{{$data->hasVendor->city or null}}" readonly="">
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Country</label>
                                        <div class="col-sm-9">
                                           
                                               <input type="text"  class="form-control"  value="{{$data->hasVendor->country or null}}" readonly="">
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Phone No</label>
                                        <div class="col-sm-9">
                                           
                                               <input type="text"  class="form-control"  value="{{$data->hasVendor->phone_no or null}}" readonly="">
                                        </div>
                                </div>
                                <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">E-Mail</label>
                                        <div class="col-sm-9">
                                           
                                               <input type="text"  class="form-control"  value="{{$data->hasVendor->email or null}}" readonly="">
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                               <div class="row">
                               @if($data->status == 1)
                                  @if(in_array('order_addproduct',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                    <button class="btn btn-primary btn-md" id="AddProduct"><i class="mdi mdi-cart-plus"></i> Add Product</button>
                                   @endif
                                @endif
                                <hr>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Detail Code</th>
                                                <th>Product Name</th>
                                                <th>Product Code</th>
                                                <th>Status</th>
                                                <th>Inch</th>
                                                <th>Width</th>
                                                <th>Height</th>
                                                <th>Satuan</th>
                                                <th>#</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data->hasOrderDetail as $index=>$row)
                                           <tr>
                                                <td>{{ $index+1 }}</td>
                                                <td>{{ $row->code }}</td>
                                                <td>{{ $row->hasProduct->name or null}}</td>
                                                <td>{{ $row->hasProduct->code or null}}</td>
                                                <td>{{ $row->hasStatus->name }}</td>
                                                <td>{{ $row->inch }}</td>
                                                <td>{{ $row->width }}</td>
                                                <td>{{ $row->height }}</td>
                                                <td>{{ $row->hasUom->name }}</td>

                                                <td>
                                                    @if(in_array('order_deleteproduct',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                                    <a href="{{ url('order/deleteproduct/')}}/{{$row->id}}/{{$data->id}}" ><button class="btn btn-danger btn-md"><i class="fas fa-trash"></i></button></a> 
                                                    @endif
                                                </td></td>
                                           </tr>
                                           @endforeach
                                        </tbody>
                                   
                                    </table>
                                </div>
                                    </div>
                                </div>
                         </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="modalEditOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><b>Edit Order {{$data->no_order}}</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                <form class="form-horizontal" method="POST" action="{{ url('order/update') }}/{{$data->id}}" autocomplete="off">
                  <div class="modal-body">
                           {{ csrf_field() }}
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Shipment Date</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="shipment_date" class="form-control{{ $errors->has('shipment_date') ? ' is-invalid' : '' }} mydatepicker" id="lname"  value="{{ \Carbon\Carbon::parse($data->shipment_date)->format('m/d/Y') }}">
                                             @if ($errors->has('shipment_date'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('shipment_date') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Vendor</label>
                                        <div class="col-sm-9">
                                            <select class="form-control{{ $errors->has('vendor_id') ? ' is-invalid' : '' }}" name="vendor_id" required>
                                                @foreach(App\Models\Vendor::all() as $index=>$row)
                                                 <option value="{{$row->id}}" @if($row->id == $data->vendor_id) selected @endif>{{$row->name}}</option>
                                                @endforeach
                                                </select>
                                            @if ($errors->has('vendor_id'))
                                              <div class="invalid-feedback">
                                                {{ $errors->first('vendor_id') }}
                                            </div>
                                            @endif
                                            
                                        </div>
                                    </div>
                          </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>
                    </form>
                </div>
              </div>
            </div>
             <!-- Modal Cancel Order -->
            <div class="modal fade" id="modalCancelOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><b>Cancel Order {{$data->no_order}}</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                <form class="form-horizontal" method="GET" action="{{ url('order/delete') }}/{{$data->id}}" autocomplete="off">
                  <div class="modal-body">
                           {{ csrf_field() }}
                               <p>Are you sure?</p>
                          </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Yes</button>
                  </div>
                    </form>
                </div>
              </div>
            </div>  
            <!-- Modal Forward Order -->
            <div class="modal fade" id="modalForwardOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><b>Send Order {{$data->no_order}}</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                <form class="form-horizontal" method="GET" action="{{ url('order/forward') }}/{{$data->id}}" autocomplete="off">
                  <div class="modal-body">
                           {{ csrf_field() }}
                               <p>Send this order?</p>
                          </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                  </div>
                    </form>
                </div>
              </div>
            </div>
             <!-- Modal Add Product -->
            <div class="modal fade" id="modalAddProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"><b>Add Product Order {{$data->no_order}}</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                <form class="form-horizontal" method="POST" action="{{ url('order/addproduct') }}" autocomplete="off">
                  <div class="modal-body">
                           {{ csrf_field() }}
                           <input type="hidden" name="order_id" value="{{$data->id}}">
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Product</label>
                                        <div class="col-sm-9">
                                              <select class="form-control{{ $errors->has('product_id') ? ' is-invalid' : '' }}" name="product_id" required>
                                                @foreach(App\Models\Product::all() as $index=>$row)
                                                 <option value="{{$row->id}}">{{$row->name}} ({{$row->code}})</option>
                                                @endforeach
                                                </select>
                                            @if ($errors->has('product_id'))
                                              <div class="invalid-feedback">
                                                {{ $errors->first('product_id') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Quantity</label>
                                        <div class="col-sm-9">
                                            <input type="number" name="qty_satuan" id="qty_satuan" onkeypress="return isNumberKey(event)" class="form-control{{ $errors->has('qty_satuan') ? ' is-invalid' : '' }}" placeholder="Ex:100000" required="">
                                             @if ($errors->has('qty_satuan'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('qty_satuan') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Unit Price</label>
                                        <div class="col-sm-9">
                                               <input type="number" name="qty_global" id="qty_global" onkeypress="return isNumberKey(event)" class="form-control{{ $errors->has('qty_global') ? ' is-invalid' : '' }}"  placeholder="Ex:100000" required="">
                                                
                                             @if ($errors->has('qty_global'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('qty_global') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>

                                     <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Amount</label>
                                        <div class="col-sm-9">
                                               <input type="text"  class="form-control"  name="qty_total" id="qty_total" value="0" readonly="">
                                        </div>
                                    </div>
                                   
                                  
                                     <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Color.No</label>
                                        <div class="col-sm-9">
                                               <input type="text" name="inch" class="form-control{{ $errors->has('inch') ? ' is-invalid' : '' }}" id="lname" placeholder='Ex:0 / C ' required="">
                                                
                                             @if ($errors->has('inch'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('inch') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Width</label>
                                        <div class="col-sm-9">
                                               <input type="number" name="width" onkeypress="return isNumberKey(event)" class="form-control{{ $errors->has('width') ? ' is-invalid' : '' }}" id="lname" placeholder="Ex:100" required="">
                                                
                                             @if ($errors->has('width'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('width') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Lenght </label>
                                        <div class="col-sm-9">
                                               <input type="number" name="height" onkeypress="return isNumberKey(event)" class="form-control{{ $errors->has('height') ? ' is-invalid' : '' }}" id="lname" placeholder="Ex:100" required="">
                                                
                                             @if ($errors->has('height'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('height') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Uom</label>
                                        <div class="col-sm-9">
                                              <select class="form-control{{ $errors->has('uom') ? ' is-invalid' : '' }}" name="uom" required>
                                                @foreach(App\Models\Uom::all() as $index=>$row)
                                                 <option value="{{$row->id}}">{{$row->name}}</option>
                                                @endforeach
                                                </select>
                                            @if ($errors->has('uom'))
                                              <div class="invalid-feedback">
                                                {{ $errors->first('uom') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                     <!-- <div class="form-group row">
                                        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Notes</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control{{ $errors->has('notes') ? ' is-invalid' : '' }}" name="notes" required=""></textarea>
                                                @if ($errors->has('notes'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('notes') }}
                                            </div>
                                            @endif

                                        </div>
                                    </div>  -->
                            
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                    </form>
                </div>
              </div>
            </div>
@endsection
@section('js')
<script src="{{ url('public/admin/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>
<script src="{{ url('public/admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
    $('#zero_config').DataTable();

    $('#editOrder').click(function(){

        $('#modalEditOrder').modal('show');
    });  
    $('#CancelOrder').click(function(){

        $('#modalCancelOrder').modal('show');
    });
    $('#ForwardOrder').click(function(){

        $('#modalForwardOrder').modal('show');
    }); 
    $('#AddProduct').click(function(){

        $('#modalAddProduct').modal('show');
    });

    jQuery('.mydatepicker').datepicker();
    

     $('input[name=qty_total]').val(accounting.formatNumber( $('input[name=qty_total]').val()))
    /* count total */ 
    $('input[name=qty_satuan], input[name=qty_global]').keyup(function() {
        var qty_satuan = $('input[name=qty_satuan]').val()-0;
        var qty_global = $('input[name=qty_global]').val()-0;
        if (qty_satuan >= 0 && qty_global >= 0)
            var total = accounting.formatNumber(qty_satuan*qty_global)
            $('input[name=qty_total]').val(total)
    });
    /* end count total */ 
     $('#qty_total').val(accounting.formatNumber( $('#qty_total').val()))
    /* count total */ 
    $('#qty_satuan, #qty_global').keyup(function() {

        var qty_satuan = $('#qty_satuan').val()-0;
        var qty_global = $('#qty_global').val()-0;
        if (qty_satuan >= 0 && qty_global >= 0)
            var total = accounting.formatNumber(qty_satuan*qty_global)
            $('#qty_total').val(total)
    });
    /* end count total */

    /* validate type number*/

    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }

    /* end validate type number*/ 


</script>
@endsection