@extends('layouts.admin')
@section('css')
<link rel="stylesheet" type="text/css" href="{{ url('public/admin/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection
@section('content')
   <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Add Order</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('order.index')}}">Order</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Order</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

	  <div class="card">

            <div class="card-body">
           			 <form class="form-horizontal" method="POST" action="{{ route('order.store') }}">
           			 	   {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">No Order</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="no_order" class="form-control{{ $errors->has('no_order') ? ' is-invalid' : '' }}" id="lname" placeholder="Ex:XXX01">
                                             @if ($errors->has('no_order'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('no_order') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                   
                                     <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Supplier</label>
                                        <div class="col-sm-9">
                                             <select class="form-control{{ $errors->has('vendor_id') ? ' is-invalid' : '' }}" name="vendor_id" required>
                                                @foreach(App\Models\Vendor::all() as $index=>$row)
                                                 <option value="{{$row->id}}">{{$row->name}}</option>
                                                @endforeach
                                                </select>
                                             @if ($errors->has('vendor_id'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('vendor_id') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div> 
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Shipment Date</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="shipment_date" class="form-control{{ $errors->has('shipment_date') ? ' is-invalid' : '' }} mydatepicker" placeholder="mm/dd/yyyy" id="lname" >
                                             @if ($errors->has('shipment_date'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('shipment_date') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- <div class="form-group row">
                                        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Notes</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control{{ $errors->has('notes') ? ' is-invalid' : '' }}" name="notes"></textarea>
                                                @if ($errors->has('notes'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('notes') }}
                                            </div>
                                            @endif

                                        </div>
                                    </div> 
                             -->

                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>

            </div>
        </div>
@endsection
@section('js')
<script src="{{ url('public/admin/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/accounting.js/0.4.1/accounting.min.js"></script>
<script type="text/javascript">
    jQuery('.mydatepicker').datepicker();
    /* count total */ 
    $('input[name=qty_satuan], input[name=qty_global]').keyup(function() {
        var qty_satuan = $('input[name=qty_satuan]').val()-0;
        var qty_global = $('input[name=qty_global]').val()-0;
        if (qty_satuan >= 0 && qty_global >= 0)
            var total = accounting.formatNumber(qty_satuan*qty_global)
            $('input[name=qty_total]').val(total)
    });
    /* end count total */ 
</script>
@endsection