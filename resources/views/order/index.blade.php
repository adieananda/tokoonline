@extends('layouts.admin')
@section('css')
<link href="{{ url('public/admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
       <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Order</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Order</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

	  <div class="card">
                    <div class="card-body">
                           @if(in_array('order_create',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                            		<a href="{{route('order.create')}}"><button class="btn btn-primary btn-md"><i class="mdi mdi-cart-plus"></i> Add Order</button></a>
                          @endif
                           @if(in_array('order_scan',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                    <button class="btn btn-success btn-md" id="scanOrder"><i class="mdi mdi-scanner"></i> Scan Order</button>
                             @endif
		                    	<hr>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Order No</th>
                                                <th>Vendor</th>
                                                <th>Shipment Date</th>
                                                <th>Created by</th>
                                                <th>Created at</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach(App\Models\Order::orderBy('created_at','desc')->get() as $index=>$row)
                                   	        <tr>
                                                <td>
                                                   @if(in_array('order_detail',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                                <a href="{{route('order.show',$row->id)}}">{{$row->no_order}}</a>
                                                  @else
                                                  {{$row->no_order}}
                                                  @endif
                                                </td>
                                               <td>{{$row->hasVendor->name or null}}</td>
                                                <td>{{\Carbon\Carbon::parse($row->shipment_date)->toDateString()}}</td>
                                                <td>{{\App\User::findOrFail($row->created_by)->name}}</td>
                                                <td>{{\Carbon\Carbon::parse($row->created_at)->toDateString()}}</td>
                                                <td>{{$row->hasStatus->name or null}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                   
                                    </table>
                                </div>

                            </div>
                        </div>

                        <!-- Modal Scan  -->
                         <div class="modal fade" id="modalScanOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle"><b>Scan Order</b></h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                        <div class="form-group row">
                                           <div class="col-sm-12">
                                                   <input type="text"  class="form-control"   id="orderScan" onkeyup="lookup(this);" placeholder="Ex:74410">
                                            </div>
                                        </div>
                                      </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                          </div>
                        </div>

@endsection
@section('js')
<script src="{{ url('public/admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
	$('#zero_config').DataTable({
        "order": []
});
     $('#scanOrder').click(function(){

        $('#modalScanOrder').modal('show');
    }); 

     function lookup(val){

         $.get( "{{ url('order/scan') }}/"+val.value, function( data ) {
                
                window.open("{{ url('order') }}/"+data.id);
                $('#modalScanOrder').modal('hide');
                $('#orderScan').val('');

         });
        
     }


</script>
@endsection