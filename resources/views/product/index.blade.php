@extends('layouts.admin')
@section('css')
<link href="{{ url('public/admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
       <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Product</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Product</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

	  <div class="card">

                            <div class="card-body">
                                 @if(in_array('product_create',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                            		<a href="{{route('product.create')}}"><button class="btn btn-primary btn-md"><i class="mdi mdi-cart-plus"></i> Add Product</button></a>
                                @endif
		                    	<hr>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Desc</th>
                                                <th>Uom</th>
                                                <th>Created At</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach(App\Models\Product::all() as $index=>$row)
                                   	        <tr>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->code}}</td>
                                                <td>{{$row->desc}}</td>
                                                <td>{{$row->hasUom->name}}</td>
                                                <td>{{$row->created_at}}</td>
                                                <td>
                                                    @if(in_array('product_delete',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                                    <a href="{{ url('product/delete/')}}/{{$row->id}}" ><button class="btn btn-danger btn-md"><i class="fas fa-trash"></i></button></a> 
                                                    @endif
                                                    @if(in_array('product_edit',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                                    <a href="{{route('product.edit',$row->id)}}"><button class="btn btn-primary btn-md"><i class="fas fa-edit"></i></button></a></td>
                                                    @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                   
                                    </table>
                                </div>

                            </div>
                        </div>

@endsection
@section('js')
<script src="{{ url('public/admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
	$('#zero_config').DataTable();
</script>
@endsection