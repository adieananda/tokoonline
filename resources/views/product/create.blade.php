@extends('layouts.admin')
@section('content')
   <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Add Product</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('product.index')}}">Product</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Add Product</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

	  <div class="card">

            <div class="card-body">
           			 <form class="form-horizontal" method="POST" action="{{ route('product.store') }}">
           			 	   {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Product Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="fname" name="name" placeholder="Ex:BAJU" required="">
                                            @if ($errors->has('name'))
	                                          <div class="invalid-feedback">
	                                            {{ $errors->first('name') }}
	                                        </div>
	                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Product Code</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="code" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" id="lname" placeholder="Ex:BJ001" required="">
                                             @if ($errors->has('code'))
	                                            <div class="invalid-feedback">
	                                            {{ $errors->first('code') }}
	                                        </div>
	                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Uom</label>
                                        <div class="col-sm-9">
                                               <select class="form-control{{ $errors->has('uom') ? ' is-invalid' : '' }}" name="uom" required>
                                           		@foreach(App\Models\Uom::all() as $index=>$row)
                                           		 <option value="{{$row->id}}">{{$row->name}}</option>
                                           		@endforeach
                                        		</select>
                                        	 @if ($errors->has('uom'))
	                                            <div class="invalid-feedback">
	                                            {{ $errors->first('uom') }}
	                                        </div>
	                                        @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Description</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control{{ $errors->has('desc') ? ' is-invalid' : '' }}" name="desc"></textarea>
	                                            @if ($errors->has('desc'))
	                                            <div class="invalid-feedback">
	                                            {{ $errors->first('desc') }}
	                                        </div>
	                                        @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>

            </div>
        </div>
@endsection