<aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('/home')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                         @if(in_array('order_view',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('order.index') }}" aria-expanded="false"><i class="mdi mdi-microsoft"></i><span class="hide-menu">Order</span></a></li>
                        @endif
                         @if(in_array('product_view',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('product.index') }}" aria-expanded="false"><i class="mdi mdi-package-variant-closed"></i><span class="hide-menu">Product</span></a></li>
                        @endif
                         @if(in_array('vendor_view',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('vendors.index') }}" aria-expanded="false"><i class="mdi mdi-bank"></i><span class="hide-menu">Supplier</span></a></li>
                        @endif
                         <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu">Settings </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                 @if(in_array('employee_view',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                 <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('user.index') }}" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Users</span></a></li>
                                 @endif
                                  @if(in_array('uom_view',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                <li class="sidebar-item"><a href="{{ route('uom.index') }}" class="sidebar-link"><i class="mdi mdi-weight"></i><span class="hide-menu"> Uom</span></a></li>
                                @endif

                            </ul>
                        </li>
                       
                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>