@extends('layouts.admin')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Create Uom</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Uom</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Create Uom</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

      <div class="card">

            <div class="card-body">
                       <form class="form-horizontal" method="POST" action="{{ route('uom.store') }}">
                             {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"  name="name" value="{{ old('name') }}" placeholder="Ex:Kg" >
                                            @if ($errors->has('name'))
                                              <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>

            </div>
        </div>

@endsection
