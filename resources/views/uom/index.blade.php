@extends('layouts.admin')
@section('css')
<link href="{{ url('public/admin/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css')}}" rel="stylesheet">
@endsection
@section('content')
       <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Uom</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Uom</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
	     <div class="card">
            <div class="card-body">
                 @if(in_array('uom_add',json_decode(\Auth::user()->hasRoles->access_permission)->data))
            		<a href="{{route('uom.create')}}"><button class="btn btn-primary btn-md"><i class="mdi mdi-weight"></i> Add Uom</button></a>
                    @endif
            	<hr>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(App\Models\Uom::all() as $index=>$row)
                   	        <tr>
                                <td>{{$index+1}}</td>
                                <td>{{$row->name}}</td>
                                <td>{{$row->created_at}}</td>
                                <td>
                                    @if(in_array('uom_delete',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                    <a href="{{ url('uom/delete/')}}/{{$row->id}}" ><button class="btn btn-danger btn-md"><i class="fas fa-trash"></i></button></a> 
                                    @endif
                                    @if(in_array('uom_edit',json_decode(\Auth::user()->hasRoles->access_permission)->data))
                                    <a href="{{route('uom.edit',$row->id)}}"><button class="btn btn-primary btn-md"><i class="fas fa-edit"></i></button></a></td>
                                    @endif
                            </tr>
                            @endforeach
                        </tbody>
                   
                    </table>
                </div>

            </div>
        </div>

@endsection
@section('js')
<script src="{{ url('public/admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
<script type="text/javascript">
	$('#zero_config').DataTable();
</script>
@endsection