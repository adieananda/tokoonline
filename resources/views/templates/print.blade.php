<!DOCTYPE html>
<html lang="en">
  <head>
     <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
     <style type="text/css">
    @font-face {
    font-family: 'DejaVu Sans';
    font-weight: normal;
    font-style: normal;
    font-variant: normal;
    /*src: url("http://eclecticgeek.com/dompdf/fonts/Elegance.ttf") format("truetype");*/
  }
  body {
    font-family: DejaVu Sans, sans-serif;
  }
    body {
      border-bottom: 15px solid #42f474;
      border-top: 15px solid #42f474;
      font-size: 0.6em;
    }
    .center{
      text-align: center;
    }
    .logo-name{
      font-size: 0.7em;
      font-weight: 90px;
      text-align: center;
    }
    .logo-title-child{
      font-size: 0.6em;
      text-align: center;
    }
    .col-md-2{
      border: 1px solid black;
      padding-left: 100px;
      padding-right: 100px;
    }
    .col-md-8{
       border: 1px solid black;
        padding-right: 224.5px;
        padding-left: 224.5px;
        background-color: lightblue;
    }
    .title{
       font-size: 1.0em;
       text-transform: uppercase;
      text-align: center;
      font-weight: bold;
      text-decoration: underline;
    }
    .title-child{
       font-size: 0.7em;
      text-align: center;
    }
    .header{
      text-align: center;
     
    }
    .clearfix{
      padding-top: 20px;
    }
    .space-col{
      padding-top: 20px;
    }
    .disclaimer{
        font-size: 0.7em;
        text-align: center;
    }
    .text-content{
        font-size: 0.7em;
    }
         #customers {
          font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
          font-size: 0.8em;
          /*border-collapse: collapse;*/
          /*width: 100%;*/
      }

      #customers td, #customers th {
          border: 1px solid #ddd;
          padding: 8px;
      }


      #customers tr:nth-child(even){background-color: #f2f2f2;}

      #customers tr:hover {background-color: #ddd;}

      #customers th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: center;
          background-color: #a6a6a6;
          color: white;
      }
      #verified{

        /*border: 1px solid #ddd;*/
        font-size: 0.8em;
      }
      #verified td, #verified th{

        padding: 10px;
        border: 1px solid #000;
      }

      .table-batch{
        margin-top: 10px;
        padding: 10px;
        border:1px solid #000
      }
      .box{
        padding-left: 10px;
        padding-right: 10px;

      }
      .table2{
         padding: 10px;
          border:1px solid #000;
          font-size:0.8em;
      }
      .table1{
       /* padding-top: 12px;
          padding-bottom: 12px;*/
          padding: 10px;
          border:1px solid #000;
          font-size:0.8em;
      }
      .table1 tr td{
          /*border: 1px solid #000;*/
          padding-top: 3px;
          padding-bottom: 3px;
      }

      .table2 tr td{
          border: 1px solid #000;

      }
      .table3 tr td{
        border:none;
      }
    .bold{
      font-weight: bold;
    }
    .table1-note{
      font-size: 0.7em;
    }
    </style>
  </head>
  <body>

     <!-- header -->
     <table style="padding-top: 20px;padding-bottom: 10px;border-bottom: 3px solid #eeeeee;"  width="1100px;" >
          <tr>
            <td   class="center"  style="width: 30%;padding: 10px;" > <img style="width:150px;height: 150px;margin-top:10px;" src="http://deatexs.com/public/fe/images/logo21.png"><td>
            <td   style="width: 70%;text-align: right;"><p>DEATEX TEXTILE<br>Jl. Cibaligo KM 1.75, Leuwigajah Utama<br> Cimahi Selatan, Utama,Cimahi Sel<br>PHONE NO (0062 022 6032710)<br>FAX (0062 022 6032710)</p></td>
            <!-- <td   class="center"  style="width: 30%;font-size: 0.6em"> <img style="float:left;padding-top: 10px;"  src="data:image/png;base64,{{DNS1D::getBarcodePNG($data['no_order'],'C93',2.7,100)}}" alt="barcode" /></td> -->
          </tr>
        
      </table>
    <!-- endheader -->

    <div class="header">
        <p style="font-size: 0.9em;text-align: justify;">} We as buyer, hereby confirm having concluded the sales contract with you (seller), as a buyer, to sell the undermentioned goods on the date and terms and conditions herein after set fourth;</p>
      </div>

       <table   width="1100px;" >
          <tr>
            <td   class="center"  style="width: 30%;padding: 10px;text-align: left;" >
              <p style="text-transform: uppercase;">MESSERS : <b>{{ \App\User::findOrFail($data['created_by'])->name }}</b>
               <br> SALES CONTRACT NO :<b>{{$data['no_order']}}</b><br>
               <br style="text-transform: uppercase;"> DATE :<b>{{\Carbon\Carbon::now()->toDateString()}}</b><br>

              </p> 
              <td>
            <td   style="width: 70%;text-align: right;">
              <p><img  src="data:image/png;base64,{{DNS1D::getBarcodePNG($data['no_order'],'C93',2.7,100)}}" alt="barcode" /></p>
             
            </td>
           </tr>
        
      </table>
    

    <!-- header -->
      <table   width="1100px;">
          
          
    
           @php
                  $global = 0;
                  $total = 0;
                @endphp
                <?php
                    //Columns must be a factor of 12 (1,2,3,4,6,12)
                    $numOfCols = 2;
                    $rowCount = 0;
                    $bootstrapColWidth = 12 / $numOfCols;
                    ?>
                     <tr>
                    <?php
                    foreach (\App\Models\OrderDetail::where('order_id',$data->id)->get() as $index=>$row){
                    ?>
                   @php
                  $global += $row->qty_global;
                  $total += $row->qty_total;
                @endphp                  
            <td   width="510px;">
            
              <table id="tb" class="table2" width="520px" style="padding:10px;">
              
                <tr>
                    <td colspan="3" style="padding:5px;"> <img style="width:70px;height: 70px;margin-top:10px;" src="http://deatexs.com/public/fe/images/logo21.png">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b style="font-size:1.2em">{{ $row->hasProduct->name }}</b></td>
                    
                </tr>
                <tr>
                    <td rowspan="4" style="width: 40%">
                    <img  src="data:image/png;base64,{{DNS1D::getBarcodePNG($row->id,'C93',4,100)}}" alt="barcode" />
                    <p>{{$data['no_order']}}</p>
                    <p><b>DEATEX'S </p>
                    </td>
                    <td style="width: 20%">Product Code</td>
                    <td style="width: 40%">{{ $row->hasProduct->code }}</td>
                </tr>
                <tr>
                    <td style="width: 20%">Color No</td>
                    <td style="width: 40%">{{ $row->inch}}</td>
                </tr>
                <tr>
                    <td style="width: 20%">Width</td>
                    <td style="width: 40%">{{ $row->width}}</td>
                </tr>
                <tr>
                    <td style="width: 20%">Length</td>
                    <td style="width: 40%">{{ $row->height}}</td>
                </tr>
                <tr>
                    <td colspan="3">Not accepting complaints or returns if the item has been cut</td>
                </tr>
               </table>
            
            <td>
            <?php
                        $rowCount++;
                        if($rowCount % $numOfCols == 0) echo '</tr><tr>';
                    }
                    ?>
          </tr>
        
      </table>
    <!-- endheader -->




  </body>
</html>
