<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{{ config('app.name') }}</title>
         <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="32x32" href="{{ url('public/fe/images/logo_fav.ico')}}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Toko Online System">
        <link href="{{ url('public/Stack/css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('public/Stack/css/stack-interface.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('public/Stack/css/socicon.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('public/Stack/css/lightbox.min.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('public/Stack/css/flickity.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('public/Stack/css/iconsmind.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('public/Stack/css/jquery.steps.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('public/Stack/css/theme.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('public/Stack/css/custom.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('public/Stack/css/font-roboto.css')}}" rel="stylesheet" type="text/css" media="all" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700%7CMerriweather:300,300i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    </head>
    <body class=" theme--bordered">
           <a id="start"></a>
        <div class="nav-container ">
            <nav class="bar bar-toggle " data-fixed-at="200">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col">
                            <div class="bar__module">
                                <a href="index.html">
                                    <!-- <img class="logo logo-dark" alt="logo" src="img/logo-dark.png" /> -->
                                    <!-- <img class="logo logo-light" alt="logo" src="img/disable" /> -->
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                        <div class="col d-flex justify-content-end">
                            <div class="bar__module">
                                <a class="menu-toggle pull-right" href="#" data-notification-link="sidebar-menu">
                                    <i class="stack-interface stack-menu"></i>
                                </a>
                            </div>
                            <!--end module-->
                        </div>
                        <!--end columns-->
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </nav>
            <div class="notification pos-right pos-top side-menu bg--white" data-notification-link="sidebar-menu" data-animation="from-right">
                <div class="side-menu__module pos-vertical-center text-right">
                    <ul class="menu-vertical">
                        <li>
                            <a class="inner-link" href="#home">Home</a>
                        </li>
                        <li>
                            <a class="inner-link" href="#about">About</a>
                        </li>
                        <li>
                            <a class="inner-link" href="#services">Services</a>
                        </li>
                        <li>
                            <a class="inner-link" href="#work">Work</a>
                        </li>
                        <li>
                            <a class="inner-link" href="#contact">Contact</a>
                        </li>
                    </ul>
                </div>
                <!--end module-->
                <div class="side-menu__module pos-bottom pos-absolute col-12 text-right">
                    <ul class="social-list list-inline text-right list--hover">
                       <li>
                                    <a href="https://www.instagram.com/deatexs/?hl=en">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://web.facebook.com/deatextile/?_rdc=1&_rdr">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/deatexs?lang=en">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="main-container">
            <section class="text-center space--sm">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-block">
                                <a href="index.html">
                                    <img alt="Image" src="{{ url('public/fe/images/logo21.png')}}" class="img-responsive" width="25%" height="25%" />
                                </a>
                            </div>
                            <h6 class="type--uppercase">Textile High Quality</h6>
                              <div class="modal-instance block">
                                <div class="video-play-icon video-play-icon--xs modal-trigger bg--primary"></div>
                                <span>Watch Overview</span>
                                <div class="modal-container">
                                    <div class="modal-content bg-dark" data-width="60%" data-height="60%">
                                        <iframe data-src="https://www.youtube.com/watch?v=S3UbB_amuc4" allowfullscreen="allowfullscreen"></iframe>
                                    </div>
                                    <!--end of modal-content-->
                                </div>
                                <!--end of modal-container-->
                            </div>
                            <ul class="social-list list-inline">
                                <li>
                                    <a href="https://www.instagram.com/deatexs/?hl=en" target="_blank">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://web.facebook.com/deatextile/?_rdc=1&_rdr" target="_blank">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/deatexs?lang=en" target="_blank">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section>
                <div class="container">
                    <div class="masonry">
                        <div class="masonry__container row">
                            <div class="masonry__item col-md-6 col-12"></div>
                            <div class="masonry__item col-md-6 col-12" data-masonry-filter="Beach">
                                <a href="{{ url('public/fe/images/kn2.jpg')}}" data-lightbox="gallery">
                                    <img alt="Image" src="{{ url('public/fe/images/kn2.jpg')}}" />
                                </a>
                            </div>
                            <!--end masonry item-->
                            <div class="masonry__item col-md-6 col-12" data-masonry-filter="Beach">
                                <a href="{ url('public/fe/images/kn1.jpg')}}" data-lightbox="gallery">
                                    <img alt="Image" src="{{ url('public/fe/images/kn1.jpg')}}" />
                                </a>
                            </div>
                            <!--end masonry item-->
                            <div class="masonry__item col-md-6 col-12" data-masonry-filter="Beach">
                                <a href="{{ url('public/fe/images/kn3.jpg')}}" data-lightbox="gallery">
                                    <img alt="Image" src="{{ url('public/fe/images/kn3.jpg')}}" />
                                </a>
                            </div>
                            <!--end masonry item-->
                            <div class="masonry__item col-md-6 col-12" data-masonry-filter="Beach">
                                <a href="{{ url('public/fe/images/kn4.jpg')}}" data-lightbox="gallery">
                                    <img alt="Image" src="{{ url('public/fe/images/kn4.jpg')}}" />
                                </a>
                            </div>
                            <!--end masonry item-->
                            <div class="masonry__item col-md-6 col-12" data-masonry-filter="Beach">
                                <a href="{{ url('public/fe/images/kn5.jpg')}}" data-lightbox="gallery">
                                    <img alt="Image" src="{{ url('public/fe/images/kn5.jpg')}}" />
                                </a>
                            </div>
                            <!--end masonry item-->
                            <div class="masonry__item col-md-6 col-12" data-masonry-filter="Beach">
                                <a href="{{ url('public/fe/images/kn4.jpg')}}" data-lightbox="gallery">
                                    <img alt="Image" src="{{ url('public/fe/images/kn4.jpg')}}" />
                                </a>
                            </div>
                            <!--end masonry item-->
                        </div>
                        <!--end of masonry container-->
                    </div>
                    <!--end masonry-->
                </div>
                <!--end of container-->
            </section>
            <footer class="text-center space--sm footer-5  ">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="heading-block">
                                <ul class="list-inline list--hover">
                                    <li>
                                        <a href="#">
                                            <span>Our Company</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Locations</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span>Products</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div>
                                <ul class="social-list list-inline list--hover">
                                     <li>
                                    <a href="https://www.instagram.com/deatexs/?hl=en">
                                        <i class="socicon socicon-instagram icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://web.facebook.com/deatextile/?_rdc=1&_rdr">
                                        <i class="socicon socicon-facebook icon icon--xs"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/deatexs?lang=en">
                                        <i class="socicon socicon-twitter icon icon--xs"></i>
                                    </a>
                                </li>
                                </ul>
                            </div>
                            <div>
                                <span class="type--fine-print">&copy;
                                    <span class="update-year"></span> deatex's.</span>
                                <a class="type--fine-print" href="#">Privacy Policy</a>
                                <a class="type--fine-print" href="#">Legal</a>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </footer>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
        <script src="{{ url('public/Stack/js/jquery-3.1.1.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/flickity.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/easypiechart.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/parallax.js')}}"></script>
        <script src="{{ url('public/Stack/js/typed.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/datepicker.js')}}"></script>
        <script src="{{ url('public/Stack/js/isotope.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/ytplayer.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/lightbox.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/granim.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/jquery.steps.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/countdown.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/twitterfetcher.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/spectragram.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/smooth-scroll.min.js')}}"></script>
        <script src="{{ url('public/Stack/js/scripts.js')}}"></script>
    </body>
</html>