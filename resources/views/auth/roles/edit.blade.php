@extends('layouts.admin')
@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Edit Roles </h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit Roles</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

      <div class="card">

            <div class="card-body">
                       <form class="form-horizontal" method="POST" action="{{ url('roles/update') }}/{{$data->id}}">
                             {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"  name="name" value="{{   $data->name }}" placeholder="Ex:Administrator" >
                                            @if ($errors->has('name'))
                                              <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                <hr>
                                    @foreach($output as $index=>$row)
                                       <h4 style="text-transform: uppercase;">{{ $index }}</h4> 
                                        @foreach($row as $key=>$value)

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <div class="custom-control custom-checkbox mr-sm-2">
                                                    <input type="checkbox" class="custom-control-input" name="roles_permission[]" value="{{$value['id'] }}-{{$value['name'] }}" id="{{$value['id'] }}" @if(in_array($value['id'],$permission)) checked @endif>
                                                    <label class="custom-control-label"  for="{{$value['id'] }}">{{$value['desc'] }}</label>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endforeach

                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>

            </div>
        </div>

@endsection
