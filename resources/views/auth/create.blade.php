@extends('layouts.admin')

@section('content')
<div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Create Employee</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('product.index')}}">Employee</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Create Employee</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

      <div class="card">

            <div class="card-body">
                       <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                             {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Employee ID</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control{{ $errors->has('emp_code') ? ' is-invalid' : '' }}"  name="emp_code" value="{{ old('emp_code') }}" placeholder="Ex:DXAD001" >
                                            @if ($errors->has('emp_code'))
                                              <div class="invalid-feedback">
                                                {{ $errors->first('emp_code') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Name</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"  value="{{ old('name') }}" placeholder="Ex:BUDI S">
                                             @if ($errors->has('name'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div> 
                                 <!--    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Photo</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="photo" value="{{ old('photo') }}" class="form-control{{ $errors->has('photo') ? ' is-invalid' : '' }}"  >
                                             @if ($errors->has('photo'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('photo') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>  -->
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">E-Mail</label>
                                        <div class="col-sm-9">
                                            <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}"  placeholder="Ex:xxx@xxx.com" >
                                             @if ($errors->has('email'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div> 
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Phone No</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="phone_no"  onkeypress="return isNumberKey(event)"  class="form-control{{ $errors->has('phone_no') ? ' is-invalid' : '' }}" value="{{ old('phone_no') }}" placeholder="Ex:08xxxxxx" >
                                             @if ($errors->has('phone_no'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('phone_no') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <label for="cono1" class="col-sm-3 text-right control-label col-form-label">Address</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address">{{ old('address') }}</textarea>
                                                @if ($errors->has('address'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('address') }}
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email1" class="col-sm-3 text-right control-label col-form-label">Access</label>
                                        <div class="col-sm-9">
                                               <select class="form-control{{ $errors->has('uom') ? ' is-invalid' : '' }}" name="access" required>
                                                @foreach(App\Models\Roles::all() as $index=>$row)
                                                 <option value="{{$row->id}}">{{$row->name}}</option>
                                                @endforeach
                                                </select>
                                             @if ($errors->has('access'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('access') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"  >
                                             @if ($errors->has('password'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Confirm Password</label>
                                        <div class="col-sm-9">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required >
                                              @if ($errors->has('password_confirmation'))
                                                <div class="invalid-feedback">
                                                {{ $errors->first('password_confirmation') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                   
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>

            </div>
        </div>

@endsection
@section('js')
<script type="text/javascript">
     /* validate type number*/

    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }

    /* end validate type number*/ 
</script>
@endsection