@extends('layouts.admin')
@section('css')
<link href="{{ url('public/admin/assets/libs/flot/css/float-chart.css')}}" rel="stylesheet">
@endsection
@section('content')       
         <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Dashboard</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">Home</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

             <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-hover">
                            <div class="box bg-cyan text-center">
                                <h1 class="font-light text-white"><i class="fab fa-first-order"></i></h1>
                                <h5 class="text-white m-b-0 m-t-5">{{ \App\Models\Order::count() }}</h5>
                                <h6 class="text-white">Total Order</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-hover">
                            <div class="box bg-danger text-center">
                                <h1 class="font-light text-white"><i class="fas fa-check-circle"></i></h1>
                                 <h5 class="text-white m-b-0 m-t-5">{{ \App\Models\Order::where('status',1)->count() }}</h5>
                                <h6 class="text-white">Order Complete</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-hover">
                            <div class="box bg-success text-center">
                                <h1 class="font-light text-white"><i class="fas fa-home"></i></h1>
                                <h5 class="text-white m-b-0 m-t-5">{{ \App\Models\Vendor::count() }}</h5>
                                <h6 class="text-white">Total Vendor</h6>
                            </div>
                        </div>
                    </div>
                     <!-- Column -->
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-hover">
                            <div class="box bg-warning text-center">
                                <h1 class="font-light text-white"><i class="fab fa-product-hunt"></i></h1>
                                <h5 class="text-white m-b-0 m-t-5">{{ \App\Models\Product::count() }}</h5>
                                <h6 class="text-white">Total Product</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Charts -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Order Chart</h5>
                                <div class="pie" style="height: 400px;"></div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Line Chart</h5>
                                <div class="bars" style="height: 400px;"></div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <!-- End Charts -->
               
               
              
            </div>
@endsection
@section('js')
 <script src="{{ url('public/admin/assets/libs/chart/matrix.interface.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/chart/excanvas.min.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/flot/jquery.flot.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/flot/jquery.flot.pie.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/flot/jquery.flot.time.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/flot/jquery.flot.crosshair.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/chart/jquery.peity.min.js')}}"></script>
    <!-- <script src="{{ url('public/admin/assets/libs/chart/matrix.charts.js')}}"></script> -->
    <script src="{{ url('public/admin/assets/libs/chart/jquery.flot.pie.min.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/chart/turning-series.js')}}"></script>
    <script src="{{ url('public/admin/dist/js/pages/chart/chart-page-init.js')}}"></script>
    <script type="text/javascript">
         var data = [];
        
         @foreach($select as $index=>$row)
             data[{{$index}}] = { label: "{{ $row->hasStatus->name }}", data: {{$row->total}} }
        @endforeach

          var pie = $.plot($(".pie"), data,{
                series: {
                    pie: {
                        show: true,
                        radius: 3/4,
                        label: {
                            show: true,
                            radius: 3/4,
                            formatter: function(label, series){
                                return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'+label+'<br/>'+Math.round(series.percent)+'%</div>';
                            },
                            background: {
                                opacity: 0.5,
                                color: '#000'
                            }
                        },
                        innerRadius: 0.2
                    },
                    legend: {
                        show: false
                    }
                }
            }); 
    </script>
@endsection
