<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('/fe/images/logo_fav.ico')}}">
    <title>{{ config('app.name') }}</title>
    <!-- Custom CSS -->
    <link href="{{ url('public/admin/assets/libs/fullcalendar/dist/fullcalendar.min.css')}}" rel="stylesheet" />
    <link href="{{ url('public/admin/assets/extra-libs/calendar/calendar.css')}}" rel="stylesheet" />
    @yield('css')
    <link href="{{ url('public/admin/dist/css/style.min.css')}}" rel="stylesheet">
   
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
   <div id="main-wrapper">
        @include('include.header') 
        @include('include.sidebar')
        <div class="page-wrapper">
        @include('include.alert')
         @yield('content')

              </div>
        </div>
    <script src="{{ url('public/admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{ url('public/admin/dist/js/jquery.ui.touch-punch-improved.js')}}"></script>
    <script src="{{ url('public/admin/dist/js/jquery-ui.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ url('public/admin/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{ url('public/admin/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ url('public/admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{ url('public/admin/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{ url('public/admin/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{ url('public/admin/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{ url('public/admin/dist/js/custom.min.js')}}"></script>
     @yield('js')
</body>

</html>